#   Copyright 2023 OAI-CM Authors
#
#   Licensed under the Apache License, Version 2.0 (the "License");
#  you may not use this file except in compliance with the License.
#   You may obtain a copy of the License at
#
#       http://www.apache.org/licenses/LICENSE-2.0
#
#   Unless required by applicable law or agreed to in writing, software
#   distributed under the License is distributed on an "AS IS" BASIS,
#   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#   See the License for the specific language governing permissions and
#   limitations under the License.
#   Contact: netsoft@eurecom.fr

# coding: utf-8

#required python version Python +3.8.X 

from setuptools import setup, find_packages

NAME = "oai_cm"
VERSION = "0.0.0"

# To install the library, run the following
#
# python setup.py build && python setup.py install
#
# prerequisite: setuptools
# http://pypi.python.org/pypi/setuptools

requirements= ['flask==2.0.3',
               'pymongo==4.1.1',
               'requests==2.27.1',
               'Unipath==1.1',
               'PyYAML==6.0',
               'Jinja2==3.0.3']

# Relative path issue in alpine
# with open("../requirements.txt") as requirement_file:
#     requirements = requirement_file.read().split()

setup(
   name=NAME,
   version=VERSION,
   description="OAI Configuration Manager",
   author_email="netsoft@eurecom.fr",
   url="",
   keywords=["OAI-CM"],
   install_requires=requirements,
   packages=find_packages(),
   include_package_data=True,
   entry_points={
       'console_scripts': ['oai_cm=oai_cm.__main__:main']},
   long_description="""\
   OAI Configuration Manager to manage the configuration of core network components. At the moment it is only used to subscribe event notifications from AMF and SMF.
   """
)
