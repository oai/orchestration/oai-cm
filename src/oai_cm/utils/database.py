#   Copyright 2023 OAI-CM Authors
#
#   Licensed under the Apache License, Version 2.0 (the "License");
#  you may not use this file except in compliance with the License.
#   You may obtain a copy of the License at
#
#       http://www.apache.org/licenses/LICENSE-2.0
#
#   Unless required by applicable law or agreed to in writing, software
#   distributed under the License is distributed on an "AS IS" BASIS,
#   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#   See the License for the specific language governing permissions and
#   limitations under the License.
#   Contact: netsoft@eurecom.fr

import pymongo

class CMDB(object):
    """CM database pointer.
    """
    def __init__(self, configuration):
        self.configuration = configuration
        self.db_client = pymongo.MongoClient(configuration['database']['host'], int(configuration['database']['port']),username=configuration['database']['user'],password=configuration['database']['password'])
        if configuration['database']['reset'] == 'yes':
            self.db_client.drop_database(configuration["database"]['name'])
            print("Reseted the Database")
        self.db = self.db_client[configuration['database']['name']]
        # create collections and indexes
        self.db_collection_amf_notification = self.db["amfnotification"]
        self.db_collection_smf_notification = self.db["smfnotification"]

    def store_amf_notification(self,data):
        try:
            self.db_collection_amf_notification.insert_one(data)
        except:
            return False
        return True

    def store_smf_notification(self,data):
        try:
            self.db_collection_smf_notification.insert_one(data)
        except:
            return False
        return True

    def get_amf_notifications(self):
        cursor = self.db_collection_amf_notification.find({}, {"_id": 0})
        notifs = []
        for doc in cursor:
            notifs.append(doc)
        return notifs

    def get_smf_notifications(self):
        cursor = self.db_collection_smf_notification.find({}, {"_id": 0})
        notifs = []
        for doc in cursor:
            notifs.append(doc)
        return notifs