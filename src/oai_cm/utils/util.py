#   Copyright 2023 OAI-CM Authors
#
#   Licensed under the Apache License, Version 2.0 (the "License");
#  you may not use this file except in compliance with the License.
#   You may obtain a copy of the License at
#
#       http://www.apache.org/licenses/LICENSE-2.0
#
#   Unless required by applicable law or agreed to in writing, software
#   distributed under the License is distributed on an "AS IS" BASIS,
#   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#   See the License for the specific language governing permissions and
#   limitations under the License.
#   Contact: netsoft@eurecom.fr


import os
import yaml
import sys
from jinja2 import Environment, FileSystemLoader
from oai_cm.utils.database import CMDB

CONFIG_FILE = str(os.getenv('CONFIG_FILE','../../../etc/configuration.yaml'))
MOUNT_CONFIG = str(os.getenv('MOUNT_CONFIG','no')).lower()

def read_yamlfile(filename):
    '''
    translates yaml to dict
    '''
    dirname = os.path.dirname(__file__)
    filename = os.path.join(dirname, filename)
    try:
        with open(filename) as f:
            v = yaml.safe_load(f)
        return v
    except Exception as e:
        print(f"Exception in reading the configuration file {e}")
        return

def render_config(CONFIG_FILE,MOUNT_CONFIG='no'):
    def render(filepath,values):
        env = Environment(loader=FileSystemLoader(os.path.dirname(filepath)))
        jinja_template = env.get_template(os.path.basename(filepath))
        template_string = jinja_template.render(env=values)
        return template_string
    #list of all the environment variables
    env_variables = {}
    for name, value in os.environ.items():
        env_variables.update({name:value})
    if MOUNT_CONFIG != "yes":
        output = render(CONFIG_FILE,env_variables)
        with open(CONFIG_FILE, "w") as fh:
            fh.write(output)
        print(f"Configuration file {CONFIG_FILE} is ready")
    else:
        print("Configuration file is mounted")

    return read_yamlfile(CONFIG_FILE)

#configuration
configuration = render_config(CONFIG_FILE,MOUNT_CONFIG)

cmdb = CMDB(configuration)