#   Copyright 2023 OAI-CM Authors
#
#   Licensed under the Apache License, Version 2.0 (the "License");
#  you may not use this file except in compliance with the License.
#   You may obtain a copy of the License at
#
#       http://www.apache.org/licenses/LICENSE-2.0
#
#   Unless required by applicable law or agreed to in writing, software
#   distributed under the License is distributed on an "AS IS" BASIS,
#   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#   See the License for the specific language governing permissions and
#   limitations under the License.
#   Contact: netsoft@eurecom.fr

import json
from flask import request
import requests
import uuid
from datetime import datetime
from oai_cm.utils.util import configuration, cmdb
from flask import Blueprint

# Blueprint
cn_event_bp = Blueprint('cn_event_bp', __name__)

## Northbound
@cn_event_bp.route('/notifications', methods=['GET'])
def notifications():
    amf = cmdb.get_amf_notifications()
    smf = cmdb.get_smf_notifications()
    data = {
        "AMF":{
            "notifications": amf
        },
        "SMF": {
            "notifications": smf
        }
    }
    return data, 200

## Northbound
@cn_event_bp.route('/subscribe/notification/amf', methods=['POST','GET'])
def subscribe_to_amf_notifications():
    data = json.loads(request.data)
    body = {
            "subscription": {
                "eventList": [
                    {
                        "type": data["event"]
                    }
                ],
                "eventNotifyUri":f"{configuration['application']['host']}:{configuration['application']['port']}/notification/amf",
                "notifyCorrelationId": str(uuid.uuid1()),
                "nfId": str(uuid.uuid1())
            }
        }
    url = f"http://{configuration['corenetwork']['amf']['host']}{configuration['corenetwork']['amf']['subscriptionUrl']}"
    r = requests.post(url=url, json=body)
    return r.text, 201

## Northbound
@cn_event_bp.route('/subscribe/notification/smf', methods=['POST', 'GET'])
def subscribe_to_smf_notifications():
    data = json.loads(request.data)
    body = {
        "anyUeInd": True,
        "groupId": "aEb1CD9b-561-97-2cbA7bEc2eAC07ECb6",
        "pduSeId": 1,
        "dnn": "oai",
        "notifId": str(uuid.uuid1()),
        "notifUri": f"{configuration['application']['host']}:{configuration['application']['port']}/notification/smf",
        "altNotifIpv4Addrs": [
            configuration['application']['host']
        ],
        "altNotifIpv6Addrs": [
            "fe80::14e0:6d4a:928e:628c"
        ],
        "altNotifFqdns": [
            "string"
        ],
        "eventSubs": [
            {
            "event": data["event"]
            }
        ],
        "eventNotifs": [
            {
            "event": data["event"],
            "timeStamp": str(datetime.utcnow().isoformat()[:-3])+'Z'
            }
        ]
    }
    url = f"http://{configuration['corenetwork']['smf']['host']}{configuration['corenetwork']['smf']['subscriptionUrl']}"
    r = requests.post(url=url,json=body)
    return r.text, 201

## Southbound to communicate with core-network functions
@cn_event_bp.route('/notification/smf', methods=['POST', 'GET'])
def smf_notification():
    data = json.loads(request.data)
    cmdb.store_smf_notification(data)
    print(data)
    return "1", 200

@cn_event_bp.route('/notification/amf', methods=['POST', 'GET'])
def amf_notification():
    data = json.loads(request.data)
    cmdb.store_amf_notification(data)
    print(data)
    return "1", 200