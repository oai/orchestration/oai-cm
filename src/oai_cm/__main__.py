#   Copyright 2023 OAI-CM Authors
#
#   Licensed under the Apache License, Version 2.0 (the "License");
#  you may not use this file except in compliance with the License.
#   You may obtain a copy of the License at
#
#       http://www.apache.org/licenses/LICENSE-2.0
#
#   Unless required by applicable law or agreed to in writing, software
#   distributed under the License is distributed on an "AS IS" BASIS,
#   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#   See the License for the specific language governing permissions and
#   limitations under the License.
#   Contact: netsoft@eurecom.fr


from flask import Flask
from oai_cm.controllers.eventExposure import cn_event_bp
from oai_cm.utils.util import configuration

def main():
    app = Flask(__name__)
    app.register_blueprint(cn_event_bp)
    app.run(port=configuration['application']['port'],host=configuration['application']['host'],threaded=True)

if __name__ == '__main__':
    main()