<table style="border-collapse: collapse; border: none;">
  <tr style="border-collapse: collapse; border: none;">
    <td style="border-collapse: collapse; border: none;">
      <a href="http://www.openairinterface.org/">
         <img src="./docs/images/oai_final_logo.png" alt="" border=3 height=50 width=150>
         </img>
      </a>
    </td>
    <td style="border-collapse: collapse; border: none; vertical-align: center;">
      <b><font size = "5">OpenAirInterface Configuration Manager </font></b>
    </td>
  </tr>
</table>

`oai-cm` is a management network function designed to manage oai-core network functions. In the future we may extend its functionality to manage the radio access network functions using o-ran O1 interface. At the moment the repository is in very early state and only supports subscribing to event notifications from `amf` and `smf`.


**TABLE OF CONTENTS**

1.  [Architecture](#1-architecture)
2.  [How to Install](#2-how-to-install)
3.  [How to use](#3-how-to-use)
4.  [How to Contribute](#4-how-to-contribute)
5.  [Leave a Feedback or Ask Questions](#5-leave-a-feedback-or-ask-questions)
6.  [Authors](#6-authors)

## 1. Architecture

At the moment `oai-cm` provides only the functionality to subscribe to event notifications from `oai-amf` and `oai-smf` using below northbound endpoints

1. Subscribe to AMF notifications via `/subscribe/notification/amf` endpoint
2. Subscribe to SMF notifications via `/subscribe/notification/smf` endpoint
3. Once the notifications are sucessfully subcribed they can be fetched from `/notifications` endpoint

### 1.1 Repository Structure

```
├── ci-scripts
│   ├── database
│   └── healthscripts
├── docker
├── docs
│   └── images
├── etc
├── src
│   ├── oai_cm
│   │   ├── controllers
│   │   │   
│   │   └── utils
```

At the moment the northbound api is using flask without any swagger defination. The routes are defined in src/oai_cm/controllers/eventExposure.py. Later we will add `openapi` specification for northbound.

## 2. How to Install

It is possible to use `oai-cm` in baremetal or via docker containers and docker-compose. Below are the instructions for both, 

### 2.1 Baremetal Installation

This kind of setup is good for development purpose. 

Pre-requisite: python +3.8, virtualenv(optional) 

**Note**: Only tested in linux at the moment

Clone the project

```shell
git clone https://gitlab.eurecom.fr/oai/orchestration/oai-cm.git
```

**Optional**: If you want to create an environment for testing then you can use `virtualenv` or `pyenv`. Here is an example with virtualenv to create a virtual python environment for testing/development. 

We assume that you run the below commands from the parent folder,

```shell
cd oai-cm
apt-get install virtualenv
virtualenv -p python3 venv
# to start the virtual environment
source venv/bin/activate
# to get out of the environment 
deactivate
```

Installing and starting the MEP,

```shell
cd oai-cm/src
pip install .
#if you want to edit the code while running you can use -e
pip install -e .
```

## 2.1.1 Configuration

Configuration is set using [configuration.yaml](./etc/configuration.yaml) file. It is written using python jinja2 template. At the runtime configuration parameters are fetched from environment variables to create the file. You can set the parameters as environment variables or fill the information in the configuration file and set the environment variable `MOUNT_CONFIG` to `yes`. A sample [configuration.yaml](./etc/configuration.sample.yaml) is present in `./etc` repository. 

Running in baremetal setup requires 
- A running mongodb instance, in sample configuration file it is configured to `localhost:27017`

To start

```shell
#current directory is oai-cm/
#set the configuration file to etc/configuration.sample.yaml, you need to set the full path
export CONFIG_FILE=<your-parent-directory>/etc/configuration.sample.yaml
# to mount the configuration file
export MOUNT_CONFIG=yes
#start the mongodb server we are using our docker compose for this 
docker compose -f ci-scripts/docker-compose.yaml up -d mongodb
oai_cm
```

Normall you can access the northbound api on `http://localhost:9999` in case `9999` port is not available on your machine then you can change in the configuration file or using environment variable `PORT`.

### 2.2 Docker Based Installation

Pre-requisite: docker server version > 20, docker-compose version> v2 

You can pull the docker image from our [official repository](https://hub.docker.com/r/oaisoftwarealliance/oai-cm) or build if you have made any changes.

### 2.2.1 Build Docker Image (optional)

We assume that you run the below commands from the parent folder,

First build the container image of the `oai-cm`:

```shell
docker build -t oai-cm:latest -f docker/Dockerfile.alpine --no-cache .
```

### 2.2.2 Start Docker Container

To start the docker container you can use our pre-set ci environment

```shell
docker compose -f ci-scripts/docker-compose.yaml up -d
```


## 3. How to use

To use the `oai-cm` it needs a running core network instance, for this you can use our ci-tesbed docker-compose based core network instance. The sample configuration file is configured to use our ci-tesbed AMF and SMF instance. In case you want to use our own instance of AMF and SMF then change the amf and smf host information in configuration file. 

Make sure your system fulfils the below requirements

1. `avx2` if you are using oai-gNB docker image else you have to build it without `avx2`
2. Minimum 4 CPU and 16 GB RAM, better to have 8 CPU same machine can have core network and ran network functions for testing
3. Docker version > 22

We assume that you run the below commands from the parent folder,

```shell
docker compose -f ci-scripts/docker-compose-core-network.yaml up -d
```

Wait till the time all the containers of the core network are healthy and running. You can check it via below command 

```shell
docker compose -f ci-scripts/docker-compose-core-network.yaml ps -a 
```

Start the `oai-cm` either as baremetal process or docker container. Choose wisely the host and port number of the `oai-cm` it should be reachable from amf and smf container or host, for amf and smf to send event notifications. If you are running docker container based installation of `oai-cm` the oai-cm-url will be `http://192.168.70.168`

Subscribe to amf event `REGISTRATION_STATE_REPORT` to get information about a newly registered UE using below curl request

```shell
curl --header "Content-Type: application/json" --request POST  --data '{"event":"REGISTRATION_STATE_REPORT"}' oai-cm-url/subscribe/notification/amf 
```

Subscribe to smf event `PDU_SES_EST` to get PDU session establishment related information using below curl request

```shell
curl --header "Content-Type: application/json" --request POST  --data '{"event":"PDU_SES_EST"}' oai-cm-url/subscribe/notification/smf 
```

Now you can use rfsimulated oai-gnb and oai-nr-ue to test that `oai-cm` recevices the notification or not. 

```shell
docker compose -f ci-scripts/docker-compose-ran.yaml up -d oai-gnb
```

Once the gNB is healthy and connected to `oai-amf` you can start the rfsim oai-nr-ue container. To check if the gnb is connected to amf use the below command

```shell
# To check if gnb is healthy
docker compose -f ci-scripts/docker-compose-ran.yaml ps -a
# To check the logs of amf
docker logs oai-amf
``` 

If gNB is connected amf logs will show

```
[2023-02-20T11:40:13.651949] [AMF] [amf_app] [info ] |----------------------------------------------------------------------------------------------------------------|
[2023-02-20T11:40:13.651952] [AMF] [amf_app] [info ] |----------------------------------------------------gNBs' information-------------------------------------------|
[2023-02-20T11:40:13.651954] [AMF] [amf_app] [info ] |    Index    |      Status      |       Global ID       |       gNB Name       |               PLMN             |
[2023-02-20T11:40:13.651959] [AMF] [amf_app] [info ] |      1      |    Connected     |         0xe000       |         gnb-rfsim        |            208, 99             | 
[2023-02-20T11:40:13.651961] [AMF] [amf_app] [info ] |----------------------------------------------------------------------------------------------------------------|
```

To start the UE

```shell
docker-compose -f ci-scripts/docker-compose-ran.yaml up -d oai-nr-ue
#To check if UE is healthy 
docker-compose -f ci-scripts/docker-compose-ran.yaml ps -a
```

Once the UE container is healthy it is connected to the network you can check that by pinging to `8.8.8.8` from the UE. 

```shell
docker exec -it rfsim5g-oai-nr-ue ping 8.8.8.8
```

To see the notifications from `oai-cm`

```shell
curl --header "Content-Type: application/json" --request GET oai-cm-url/notifications
```

To remove all the containers

```shell
docker-compose -f ci-scripts/docker-compose-ran.yaml down -t2
docker-compose -f ci-scripts/docker-compose-core-network.yaml down -t2
docker-compose -f ci-scripts/docker-compose.yaml down -t2
```


## 4. How to Contribute

You want to contribute we would love to have you on-board, 

Please refer to the steps described on our website: [How to contribute to OAI](https://www.openairinterface.org/?page_id=112)

1. Sign and return a Contributor License Agreement to OAI team.
2. Create an account on [Eurecom GiLab Server](https://gitlab.eurecom.fr/users/sign_in) if you do not have any.
   - If your email domain (`@domain.com`) is not whitelisted, please contact us (mailto:contact@openairinterface.org).
   - Eurecom GitLab does NOT accept public email domains.
3. Provide the `username` of this account to the OAI team (mailto:contact@openairinterface.org) so you have developer rights on this repository.
4. The policies are described in these wiki pages: [OAI Policies](https://gitlab.eurecom.fr/oai/openairinterface5g/wikis/oai-policies-home)
   - You can fork onto another hosting system. But we will **NOT** accept a pull request from a forked repository.
      * The Continuous Integration will reject your pull request.
   - All pull requests SHALL have **`master`** branch as target branch.

## 5. Leave a Feedback or Ask Questions

If you have questions or want to leave a feedback feel free to send us an email at `netsoft@eurecom.fr`

## 6. Authors

```
Mohamed MEKKI
Karim BOUTIBA
Giulio CAROTA
Adlen KSENTINI
Stefano ROSSATO
Abderrahmen TLILI
Sagar ARORA
```
